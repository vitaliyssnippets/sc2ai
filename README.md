# README #

### What is this repository for? ###

This repo includes AI agents using pysc2 for learning to play StarCraft II scenarios.

### How do I run agents? ###
Follow the official instructions to setup your environment from the pysc2 public repo. You can run my agents with something like:
D:/Python354/envs/sc2ai/Scripts/python.exe  -m pysc2.bin.agent --max_agent_steps 1000000 --map DefeatRoaches --agent smart_agent_defeat_roaches.DefeatRoachesAgent --norender