import random
import math
import os.path

import numpy as np
import pandas as pd
import time

from pysc2.agents import base_agent
from pysc2.lib import actions
from pysc2.lib import features
from sklearn.cluster import KMeans

_NO_OP = actions.FUNCTIONS.no_op.id
_SELECT_POINT = actions.FUNCTIONS.select_point.id
_SELECT_ARMY = actions.FUNCTIONS.select_army.id
_MOVE_SCREEN = actions.FUNCTIONS.Move_screen.id
_ATTACK_SCREEN = actions.FUNCTIONS.Attack_screen.id

_PLAYER_RELATIVE = features.SCREEN_FEATURES.player_relative.index
_UNIT_TYPE = features.SCREEN_FEATURES.unit_type.index
_PLAYER_ID = features.SCREEN_FEATURES.player_id.index

_PLAYER_SELF = 1
_PLAYER_HOSTILE = 4

_TERRAN_MARINE = 48
_ZERG_ROACH = 110

_NOT_QUEUED = [0]
_QUEUED = [1]

DATA_FILE = 'defeat_roaches_data'

ACTION_DO_NOTHING = 'donothing'
ACTION_SELECT_ARMY = 'selectarmy'
ACTION_ATTACK = 'attack'
ACTION_ATTACK_TOP = 'attack_0'
ACTION_ATTACK_BOT = 'attack_1'
ACTION_MOVE_ARMY_VERT = 'movearmyv'
ACTION_MOVE_ARMY_HORI = 'movearmyh'

smart_actions = [
    ACTION_DO_NOTHING,
    ACTION_SELECT_ARMY,
    ACTION_MOVE_ARMY_VERT,
    ACTION_MOVE_ARMY_HORI,
    ACTION_ATTACK_TOP,
    ACTION_ATTACK_BOT,
]

ROACH_REWARD = 1
MARINE_REWARD = -1

# Stolen from https://github.com/MorvanZhou/Reinforcement-learning-with-tensorflow
class QLearningTable:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.9):
        self.actions = actions  # a list
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)

    def choose_action(self, observation):
        self.check_state_exist(observation)

        if np.random.uniform() < self.epsilon:
            # choose best action
            state_action = self.q_table.ix[observation, :]

            # some actions have the same value
            state_action = state_action.reindex(np.random.permutation(state_action.index))

            action = state_action.idxmax()
        else:
            # choose random action
            action = np.random.choice(self.actions)

        return action

    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        self.check_state_exist(s)

        q_predict = self.q_table.ix[s, a]
        q_target = r + self.gamma * self.q_table.ix[s_, :].max()

        # update
        self.q_table.ix[s, a] += self.lr * (q_target - q_predict)

    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series([0] * len(self.actions), index=self.q_table.columns, name=state))


class DefeatRoachesAgent(base_agent.BaseAgent):
    def __init__(self):
        super(DefeatRoachesAgent, self).__init__()

        self.qlearn = QLearningTable(actions=list(range(len(smart_actions))))

        self.previous_roaches_count = 0
        self.previous_marines_count = 0
        self.started_left = False
        self.previous_action = None
        self.previous_state = None

        if os.path.isfile(DATA_FILE + '.gz'):
            self.qlearn.q_table = pd.read_pickle(DATA_FILE + '.gz', compression='gzip')

    def step(self, obs):
        super(DefeatRoachesAgent, self).step(obs)

        unit_type = obs.observation['screen'][_UNIT_TYPE]

        marines_y, marines_x = (unit_type == _TERRAN_MARINE).nonzero()
        marines_count = int(math.ceil(len(marines_y) / 9))
        roaches_y, roaches_x = (unit_type == _ZERG_ROACH).nonzero()
        roaches_count = int(math.ceil(len(roaches_y) / 11))

        if obs.last():
            reward = 0
            if roaches_count < 4:
                reward += (4-roaches_count) * 10
            reward += marines_count - 9
            self.qlearn.learn(str(self.previous_state), self.previous_action, reward, 'terminal')
            self.qlearn.q_table.to_pickle(DATA_FILE + '.gz', 'gzip')
            self.previous_action = None
            self.previous_state = None
            self.move_number = 0
            self.previous_roaches_count = 0
            self.previous_marines_count = 0
            return actions.FunctionCall(_NO_OP, [])

        if obs.first():
            if marines_x.mean() < roaches_x.mean():
                self.started_left = True

        roaches = []
        for i in range(0, len(roaches_y)):
            roaches.append((roaches_x[i], roaches_y[i]))
        kmeans = KMeans(n_clusters=roaches_count)
        kmeans.fit(roaches)
        roaches = []
        for i in range(0, len(kmeans.cluster_centers_)):
            roaches.append((kmeans.cluster_centers_[i][0], kmeans.cluster_centers_[i][1]))
        roaches.sort(key=lambda r: r[1])

        hot_squares = np.zeros(64)
        ally_squares = np.zeros(64)
        current_state = np.zeros(2 + len(hot_squares) + len(ally_squares))
        current_state[0] = marines_count
        current_state[1] = roaches_count

        enemy_y, enemy_x = (obs.observation['minimap'][_PLAYER_RELATIVE] == _PLAYER_HOSTILE).nonzero()
        for i in range(0, len(enemy_y)):
            y = int(math.ceil((enemy_y[i] + 1) / 8))
            x = int(math.ceil((enemy_x[i] + 1) / 8))
            hot_squares[((y - 1) * 8) + (x - 1)] = 1
        if not self.started_left:
            hot_squares = hot_squares[::-1]
        for i in range(0, len(hot_squares)):
            current_state[i + 2] = hot_squares[i]

        enemy_y, enemy_x = (obs.observation['minimap'][_PLAYER_RELATIVE] == _PLAYER_SELF).nonzero()
        for i in range(0, len(enemy_y)):
            y = int(math.ceil((enemy_y[i] + 1) / 8))
            x = int(math.ceil((enemy_x[i] + 1) / 8))
            ally_squares[((y - 1) * 8) + (x - 1)] = 1
        if not self.started_left:
            ally_squares = ally_squares[::-1]
        for i in range(0, len(ally_squares)):
            current_state[i + 2 + 64] = ally_squares[i]

        if self.previous_action is not None:
            reward = 0
            if roaches_count < self.previous_roaches_count:
                reward += 10
            reward += marines_count - self.previous_marines_count
            self.qlearn.learn(str(self.previous_state), self.previous_action, reward, str(current_state))

        rl_action = self.qlearn.choose_action(str(current_state))
        smart_action = smart_actions[rl_action]

        self.previous_roaches_count = roaches_count
        self.previous_marines_count = marines_count
        self.previous_state = current_state
        self.previous_action = rl_action

        j = 0
        if '_' in smart_action:
            smart_action, j = smart_action.split('_')
            j = int(j)

        if smart_action == ACTION_DO_NOTHING:
            return actions.FunctionCall(_NO_OP, [])

        elif smart_action == ACTION_SELECT_ARMY:
            if _SELECT_ARMY in obs.observation['available_actions']:
                return actions.FunctionCall(_SELECT_ARMY, [_NOT_QUEUED])

        elif smart_action == ACTION_ATTACK and roaches_count > 0:
            if _ATTACK_SCREEN in obs.observation["available_actions"]:
                if j != 0:
                    j = len(roaches) - 1
                target = roaches[j]
                return actions.FunctionCall(_ATTACK_SCREEN, [_NOT_QUEUED, target])

        return actions.FunctionCall(_NO_OP, [])